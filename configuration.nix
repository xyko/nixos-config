{ config, pkgs, ... }:

{
  imports = [
    ./cups.nix
    ./hardware-configuration.nix
    ./home-manager.nix
    ./kernel.nix
    ./net.nix
    ./sane.nix
    ./sshd.nix
    ./thermals.nix
    ./vim.nix
    ./zsh.nix
  ];

  # Bootloader
  boot.loader = {
    efi = {
      canTouchEfiVariables = true;
      efiSysMountPoint = "/boot/efi";
    };
    grub = {
      enable       = true;
      efiSupport   = true;
      version      = 2;
      device       = "nodev";
      extraEntries = ''
        menuentry "Starcraft 2" {
          insmod part_gpt
          search --no-floppy --set=root --label EFI
          chainloader /EFI/Microsoft/Boot/bootmgfw.efi
        }
      '';
    };
  };

  system.autoUpgrade = {
      enable             = true;
      operation          = "switch";
      channel            = "https://nixos.org/channels/nixos-unstable";
      randomizedDelaySec = "120min";
  };

  # Set your time zone.
  time.timeZone = "Europe/Berlin";

  # Select non-americanization properties.
  i18n.defaultLocale = "en_US.UTF-8";
  i18n.supportedLocales = [
    "en_US.UTF-8/UTF-8"
    "de_DE.UTF-8/UTF-8"
    "pt_BR.UTF-8/UTF-8"
  ];
  console = {
    font = "Lat2-Terminus16";
    useXkbConfig = true;
    earlySetup = true;
    packages = with pkgs; [ terminus_font ];
  };

  # Enable the X11 windowing system.
  services.xserver.enable = true;
  services.xserver.displayManager.sddm.enable = true;
  services.xserver.desktopManager.plasma5.enable = true;
  services.openssh.settings.X11Forwarding = true;

  # NVidia
  services.xserver.videoDrivers = [ "nvidia" ];
  hardware.opengl.enable = true;
  #hardware.nvidia.package = config.boot.kernelPackages.nvidiaPackages.vulkan_beta;
  hardware.nvidia.package = config.boot.kernelPackages.nvidiaPackages.stable;

  # Firmwares
  hardware.enableAllFirmware = true;
  hardware.enableRedistributableFirmware = true;
  hardware.cpu.amd.updateMicrocode = true;
  services.fwupd.enable = true;

  # Configure keymap in X11
  # TODO: fix cedille
  services.xserver.layout = "us";
  services.xserver.xkbVariant = "intl";
  services.xserver.xkbOptions = "eurosign:e";

  # Enable sound.
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    #jack.enable = true;
  };

  # Bluetooth
  hardware.bluetooth.enable = true;

  # Docker
  virtualisation.docker = {
    enable = true;
    storageDriver = "btrfs";
    enableNvidia = true;
    autoPrune = {
      enable = true;
      dates = "daily";
    };
  };

  # Packages
  nixpkgs.config = {
    allowUnfree = true;
  };
  nix.settings.experimental-features = [ "nix-command" ];

  environment.variables = rec {
    # - don't page if content fits the screen
    # - allow ASCII colors
    # - don't clean screen after paging
    LESS  = "--quit-if-one-screen --no-init --RAW-CONTROL-CHARS $LESS"; # don't page if content fits the screen
  };

  # User accounts
  users.users.xyko = {
    isNormalUser = true;
    shell = pkgs.zsh;
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIM4Gfs7QwhpkGM2zFdxxy2tHfusAiZBCnjhblbwzzHuo xyko@guldukat 2021-01"
    ];
    extraGroups = [
      "audio"
      "docker"
      "kvm"
      "lp"
      "networkmanager"
      "nm-openvp"
      "scanner"
      "vboxsf"
      "vboxusers"
      "wheel"
    ];
    packages = with pkgs; [
      direnv
      gcsfuse
      gnome_mplayer
      gocryptfs
      golangci-lint
      google-chrome-beta
      google-cloud-sdk
      hcloud
      kubectl
      minikube
      skaffold
      slack
      nodePackages.yaml-language-server
    ];
  };

  users.users.lu = {
    isNormalUser = true;
    shell = pkgs.zsh;
    extraGroups = [
      "audio"
      "lp"
      "networkmanager"
      "scanner"
    ];
    packages = with pkgs; [
      google-chrome-beta
    ];
  };

  # Sudo
  security.sudo.extraRules= [{
    groups = [ "wheel" ];
    commands = [{
        command = "ALL" ;
        options = [ "NOPASSWD" ];
      }];
  }];


  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    cargo
    file
    firefox
    fzf
    gcc
    git
    gnumake
    gnupg
    go
    gotools
    gocr
    htop
    inconsolata
    jq
    lm_sensors
    ncurses
    nix-index
    nix-zsh-completions
    nodejs
    openssl
    pkg-config
    plasma-pa
    powerline-go
    python3
    rustc
    slack
    smem
    sudo
    timeshift
    tmux
    unixtools.fdisk
    unzip
    usbutils
    xclip
    xsane
    zsh
    zsh-completions
    zsh-git-prompt

    gsmartcontrol
    liquidctl
    psensor

    glxinfo
    vulkan-tools
  ];

  nix.gc = {
    automatic = true;
    dates = "10:00";
    options = "--delete-older-than 5d";
  };

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  programs.mtr.enable = true;
  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true;
    dedicatedServer.openFirewall = true;
  };

  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };

  programs.nix-ld.enable = true;
  programs.droidcam.enable = true;

  # List services that you want to enable:

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  system.copySystemConfiguration = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.11"; # Did you read the comment?

}

