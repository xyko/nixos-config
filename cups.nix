{ pkgs, ... }:
{
  # Enable CUPS to print documents.
  services.avahi = {
    enable = true;
    openFirewall = true;
    nssmdns = true;
  };
  services.printing = {
    enable = true;
    drivers = [ pkgs.hplipWithPlugin ];
  };
}
