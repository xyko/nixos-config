{ pkgs, ... }:
{
  users.defaultUserShell = pkgs.zsh;
  environment.shells = with pkgs; [ zsh ];

  # make zsh usable by root
  programs.zsh = {
    enable = true;
    shellAliases = {
      bazel = "nix-shell --run bazelisk";
      update = "sudo nixos-rebuild switch";
      ls = "ls --color=auto";
      grep = "grep --color=auto";
    };
    histSize = 1000000;
    ohMyZsh = {
      enable = true;
      theme = "eastwood";
      plugins = [
          "dotenv"
          "fzf"
          "git"
          "golang"
          "kubectl"
          "thefuck"
      ];
    };
  };
}

