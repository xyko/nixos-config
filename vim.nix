{ pkgs, ... }:
{
  programs.neovim = {
    enable = true;
    defaultEditor = true;
    vimAlias = true;
    withNodeJs = true;
    configure = {
      packages.myPlugins = with pkgs.vimPlugins; {
        start = [
          syntastic
          editorconfig-vim
          vim-airline
          vim-lastplace

          # go
          coc-go
          vista-vim

          # nix
          vim-nix

          # rust
          rust-vim

          # bazel
          vim-maktaba
          vim-bazel


          # misc code stuff
          coc-diagnostic
          coc-docker
          coc-nginx
          coc-nvim
          coc-sh
          coc-toml
          coc-yaml
          syntastic
          vim-hcl
        ];
        opt = [ ];
      };
      customRC = ''
        let g:go_def_mode='gopls'
        let g:go_info_mode='gopls'

        set nocompatible
        set spell spelllang=en
        set hidden
        set nobackup
        set nowritebackup
        set cmdheight=2
        set updatetime=300
        set ignorecase
        filetype plugin indent on

        autocmd BufWritePre * :%s/\s\+$//e " remove trailing whitespaces

        " COLORS
        set termguicolors
        set cc=80
        highlight  ColorColumn   guibg=#021203
        highlight! CocErrorSign  guifg=#ffffff
        highlight! NormalFloat   guibg=#5f704f
        highlight! CocErrorFloat ctermfg=0

        " GO
        nmap <silent>             [g        <Plug>(coc-diagnostic-prev)
        nmap <silent>             ]g        <Plug>(coc-diagnostic-next)
        nmap <silent>             gd        <Plug>(coc-definition)
        nmap <silent>             gr        <Plug>(coc-references)
        nmap <silent>             gy        <Plug>(coc-type-definition)
        nmap <silent>             gi        <Plug>(coc-implementation)
        nmap                      <F2>      <Plug>(coc-rename)
        nnoremap <silent><nowait> <space>a  :<C-u>CocList diagnostics<cr>

        autocmd CursorHold * silent call CocActionAsync('highlight')

        nnoremap <silent> K :call <SID>show_documentation()<CR>
        function! s:show_documentation()
          if (index(['vim','help'], &filetype) >= 0)
            execute 'h '.expand('<cword>')
          else
            call CocAction('doHover')
          endif
        endfunction

        au BufWritePre *.go :silent call CocAction('runCommand', 'editor.action.organizeImport')
        autocmd BufWritePre *.go call GoPrepareToSave()
        fun GoPrepareToSave()
          "call CocAction('runCommand', 'editor.action.organizeImport')
          call CocAction('format')
        endfun

        " Use tab for trigger completion with characters
        inoremap <silent><expr> <TAB>
          \ coc#pum#visible() ? coc#pum#next(1) :
          \ CheckBackspace() ? "\<Tab>" :
          \ coc#refresh()
        inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"

        " Make <CR> to accept selected completion item or notify coc.nvim to format
        " <C-g>u breaks current undo, please make your own choice
        inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
                                      \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"
        function! CheckBackspace() abort
          let col = col('.') - 1
          return !col || getline('.')[col - 1]  =~# '\s'
        endfunction

        " Use <c-space> to trigger completion
        if has('nvim')
          inoremap <silent><expr> <c-space> coc#refresh()
        else
          inoremap <silent><expr> <c-@> coc#refresh()
        endif

        " RUST
        let g:rustfmt_autosave = 1
        set statusline+=%#warningmsg#
        set statusline+=%{SyntasticStatuslineFlag()}
        set statusline+=%*

        let g:syntastic_always_populate_loc_list = 1
        let g:syntastic_auto_loc_list = 1
        let g:syntastic_check_on_open = 1
        let g:syntastic_check_on_wq = 0

        " VISTA
        let g:vista#renderer#enable_icon = 1
        let g:vista_default_executive = 'coc'
        let g:vista_sidebar_width = 40
        map <F8> :Vista!!<CR>

        " YAML
        autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
      '';
    };
  };
}
