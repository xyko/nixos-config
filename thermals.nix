{ pkgs, ... }:
{
  services.thermald = {
    enable = true;
    debug  = true;
  };

  powerManagement = {
    enable = true;
    cpuFreqGovernor = "ondemand";
  };

  # https://nixos.wiki/wiki/Linux_kernel
  #boot.kernelPatches = [ {
  #      name = "amd-pstate-config";
  #      patch = null;
  #      extraConfig = ''
  #        X86_AMD_PSTATE m
  #      '';
  #} ];
}

