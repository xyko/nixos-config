{ pkgs, ... }:
{
  networking = {
    hostName = "glados";
    nameservers = [
      "8.8.8.8"
      "8.8.4.4"
      "2001:4860:4860::8888"
      "2001:4860:4860::8844"
    ];
    networkmanager = {
      enable = true;
      dns = "dnsmasq";
    };
    firewall = {
      enable = true;
      allowedTCPPorts = [];
      allowedUDPPorts = [];
    };
  };

  #services.ddclient = {
  #  enable       = true;
  #  domains      = ["glados.kurpiel.eng.br"];
  #  server       = "dynv6.com";
  #  username     = "none";
  #  passwordFile = "/home/xyko/.secret/dyndns/glados.pwd";
  #  ipv6         = true;
  #  use          = "if, if=enp14s0";
  #  verbose      = true;
  #};
}
