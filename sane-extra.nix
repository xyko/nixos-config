# from
#   https://raw.githubusercontent.com/michalrus/dotfiles/d943be3089aa436e07cea5f22d829402936a9229/.nixos-config.symlink/modules/sane-extra-config.nix
#
# as recommended by
#   https://nixos.wiki/wiki/Scanners
#
# After a while I gave up trying to understand it and just blindly used it.

{ config, lib, pkgs, ... }:
with lib;

let

  cfg = config.hardware.sane;

  pkg = if cfg.snapshot
    then pkgs.sane-backends-git
    else pkgs.sane-backends;

  backends = [ pkg ] ++ cfg.extraBackends;

  saneConfig = pkgs.mkSaneConfig { paths = backends; };

  saneExtraConfig = pkgs.runCommand "sane-extra-config" {} ''
    cp -Lr '${pkgs.mkSaneConfig { paths = [ pkgs.sane-backends ]; }}'/etc/sane.d $out
    chmod +w $out
    ${concatMapStrings (c: ''
      f="$out/${c.name}.conf"
      [ ! -e "$f" ] || chmod +w "$f"
      cat ${builtins.toFile "" (c.value + "\n")} >>"$f"
      chmod -w "$f"
    '') (mapAttrsToList nameValuePair cfg.extraConfig)}
    chmod -w $out
  '';

in

{
  options = {
    hardware.sane.extraConfig = mkOption {
      type = types.attrsOf types.lines;
      default = {};
      example = { "some-backend" = "# some lines to add to its .conf"; };
    };
  };

  config = mkIf (cfg.enable && cfg.extraConfig != {}) {
    hardware.sane.configDir = saneExtraConfig.outPath;
  };
}
