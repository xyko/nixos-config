{ pkgs, ... }:
{
  imports = [
    ./sane-extra.nix
  ];
  hardware.sane = {
    enable = true;
    extraBackends = [
      pkgs.sane-airscan
      pkgs.hplipWithPlugin
    ];
  };
}
