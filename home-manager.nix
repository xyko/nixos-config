{ config, pkgs, ... }:

let
  home-manager = builtins.fetchTarball "https://github.com/nix-community/home-manager/archive/master.tar.gz";
in
{
  imports = [
    (import "${home-manager}/nixos")
  ];

  environment.pathsToLink = [ "/share/zsh" ];

  home-manager.users.xyko = {
    home.stateVersion = "22.11";

    programs.direnv = {
      enable = true;
      nix-direnv.enable = true;
    };

    home.packages = with pkgs; [
      thefuck
      rnix-lsp                    # nix  language server
      gopls                       # go   language server
      shellcheck                  # bash language server
      black                       # python code formatter
      isort                       # organizing python imports
      shfmt                       # auto-format bash code
      yamllint
      nodePackages.write-good     # linter for english prose
      php82Packages.phpstan
      php82Packages.psalm
    ];

    programs.git = {
      enable = true;
      userName  = "Francisco Delmar Kurpiel";
      userEmail = "francisco@kurpiel.eng.br";
      aliases = {
        authors = "shortlog -n -s";
        ci = "commit -v";
        l = "log --graph --abbrev-commit --stat --pretty=fuller --decorate";
        lb = "for-each-ref --sort=-committerdate refs/heads/ --format='%(HEAD) %(color:green)%(refname:short)%(color:reset) %(color:black)### %(color:reset)%(color:yellow)%(contents:subject)%(color:reset)' --count=10";
        ld = "log --first-parent --decorate=full --pretty=tformat:'%B%n%w(0)-------------------------------------------------------------------------------";
      };
    };

    # TODO: try powerlevel10k instead
    programs.starship = {
      enable = true;
      enableZshIntegration = true;
      settings = {
        add_newline = true;
      };
    };

    programs.dircolors = {
      enable = true;
      enableZshIntegration = true;
      extraConfig = ''
        TERM xterm-256color
      '';
    };

    programs.zsh = {
      enable = true;
      dotDir = ".config/zsh";
      enableAutosuggestions = true;
      enableCompletion = true;
      enableSyntaxHighlighting = true;
      history = {
        size = 1000000;
        save = 1000000;
        ignoreSpace = true;
        share = true;
      };
      #plugins = with pkgs; [
      #  zplug = {
      #    enable = true;
      #    plugins = [
      #      { name = "romkatv/powerlevel10k"; tags = [ as:theme depth:1 ]; }
      #    ];
      #  };
      #];
    };

    # https://github.com/nix-community/home-manager/blob/master/modules/programs/neovim.nix
    programs.neovim = {
      enable = true;
      defaultEditor = true;
      plugins = with pkgs.vimPlugins; [
        editorconfig-nvim
        syntastic
        vim-airline
        vim-lastplace

        # coc
        coc-diagnostic
        coc-docker
        coc-nginx
        coc-sh
        coc-toml
        coc-yaml
        coc-markdownlint

        # go
        coc-go
        vista-vim

        # nix
        vim-nix

        # rust
        rust-vim

        # bazel
        vim-maktaba
        vim-bazel

        # misc code stuff
        syntastic
        vim-hcl

      ];
      extraConfig = ''
        let g:go_def_mode='gopls'
        let g:go_info_mode='gopls'

        set nocompatible
        set spell spelllang=en
        set hidden
        set nobackup
        set nowritebackup
        set cmdheight=2
        set updatetime=300
        set ignorecase
        filetype plugin indent on

        autocmd BufWritePre * :%s/\s\+$//e " remove trailing whitespaces

        " COLORS
        set termguicolors
        set cc=80
        highlight  ColorColumn   guibg=#021203
        highlight! CocErrorSign  guifg=#ffffff
        highlight! NormalFloat   guibg=#5f704f
        highlight! CocErrorFloat ctermfg=0

        " GO
        nmap <silent>             [g        <Plug>(coc-diagnostic-prev)
        nmap <silent>             ]g        <Plug>(coc-diagnostic-next)
        nmap <silent>             gd        <Plug>(coc-definition)
        nmap <silent>             gr        <Plug>(coc-references)
        nmap <silent>             gy        <Plug>(coc-type-definition)
        nmap <silent>             gi        <Plug>(coc-implementation)
        nmap                      <F2>      <Plug>(coc-rename)
        nnoremap <silent><nowait> <space>a  :<C-u>CocList diagnostics<cr>

        autocmd CursorHold * silent call CocActionAsync('highlight')

        nnoremap <silent> K :call <SID>show_documentation()<CR>
        function! s:show_documentation()
          if (index(['vim','help'], &filetype) >= 0)
            execute 'h '.expand('<cword>')
          else
            call CocAction('doHover')
          endif
        endfunction

        au BufWritePre *.go :silent call CocAction('runCommand', 'editor.action.organizeImport')
        "autocmd BufWritePre *.go call GoPrepareToSave()
        "fun GoPrepareToSave()
        "  call CocAction('runCommand', 'editor.action.organizeImport')
        "  call CocAction('format')
        "endfun

        " Use tab for trigger completion with characters
        inoremap <silent><expr> <TAB>
          \ coc#pum#visible() ? coc#pum#next(1) :
          \ CheckBackspace() ? "\<Tab>" :
          \ coc#refresh()
        inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"

        " Make <CR> to accept selected completion item or notify coc.nvim to format
        " <C-g>u breaks current undo, please make your own choice
        inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
                                      \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"
        function! CheckBackspace() abort
          let col = col('.') - 1
          return !col || getline('.')[col - 1]  =~# '\s'
        endfunction

        " Use <c-space> to trigger completion
        if has('nvim')
          inoremap <silent><expr> <c-space> coc#refresh()
        else
          inoremap <silent><expr> <c-@> coc#refresh()
        endif

        " RUST
        let g:rustfmt_autosave = 1
        set statusline+=%#warningmsg#
        set statusline+=%{SyntasticStatuslineFlag()}
        set statusline+=%*

        let g:syntastic_always_populate_loc_list = 1
        let g:syntastic_auto_loc_list = 1
        let g:syntastic_check_on_open = 1
        let g:syntastic_check_on_wq = 0

        " VISTA
        let g:vista#renderer#enable_icon = 1
        let g:vista_default_executive = 'coc'
        let g:vista_sidebar_width = 40
        map <F8> :Vista!!<CR>

        " YAML
        autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
      '';
      coc = {
        enable = true;
        settings = {
          languageserver = {
            go = {
               command = "gopls";
               rootPatterns = [ "go.mod" ];
               filetypes = [ "go" ];
            };

            nix = {
              command = "rnix-lsp";
              filetypes = [ "nix" ];
            };

            bash = {
              command = "bash-language-server";
              args = [ "start" ];
              filetypes = [ "sh" "inc" ];
              ignoredRootPaths = [ "~" ];
            };
          };

          "go.goplsPath" = "/home/xyko/.nix-profile/bin/gopls";
          "go.goplsOptions" = {
            local       = "sisu.sh";
            staticcheck = true;
          };

          "yaml.format.enable" = false;
          "yaml.validate" = true;
          "yaml.yamlVersion" = "1.2";
          "yaml.completion" = true;
          "yaml.schemaStore.enable" = true;
          "yaml.schemas" = {
            kubernetes = [ "/*.k8s.yaml" ];
            "https://json.schemastore.org/kustomization.json" = [ "kustomization.yaml" ];
            "https://raw.githubusercontent.com/GoogleContainerTools/skaffold/master/docs/content/en/schemas/v2beta8.json" = [ "skaffold.yaml" ];
          };

          "json.schemaDownload.enable" = true;

          "coc.preferences.colorSupport" = true;
          "coc.preferences.formatOnSaveFiletypes" = ["go"];

          "codeLens.enable" = true;

          "diagnostic-languageserver.filetypes" = {
            markdown = [ "write-good" "markdownlint" ];
            sh       = [ "shellcheck" ];
            php      = [ "phpstan" "psalm" ];
            yaml     = [ "yamllint" ];
          };

          "diagnostic-languageserver.linters" = {
            shellcheck = {
              args = [
                "--format=gcc"
                "--external-sources"
                "-"
              ];
              command    = "shellcheck";
              debounce   = 100;
              sourceName = "shellcheck";
            };
          };

          "diagnostic-languageserver.formatFiletypes" = {
            python   = [ "black" "isort"];
            sh       = [ "shfmt"];
            blade    = [ "blade-formatter" ];
            cmake    = [ "cmake-format" ];
          };
        };
      };
    };
  };
}
